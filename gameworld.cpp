#include "gameworld.h"

#include "physicsmanager.h"
#include "objects/gravitysource.h"
#include "objects/ship.h"

GameWorld::GameWorld(QObject *parent) : QObject(parent)
{
    initPhysicsManager();
}

GameWorld::~GameWorld()
{
    destroyPhysicsManager();
}

void GameWorld::addGravitySource(const QPointF &position, const qreal &radius, const qreal &mass, const bool &dynamic, const QPointF &velocity)
{
    GravitySource * gravitySource = new GravitySource(this);
    gravitySource->setMass(mass);
    gravitySource->setPosition(position);
    gravitySource->setRadius(radius);
    gravitySource->setVelocityVector(velocity);

    _gravitySources.append(gravitySource);

    _physicsManager->addGravitySource(gravitySource, dynamic);

    emit gravitySourceAdded(gravitySource);
}

void GameWorld::addShip(const QPointF &position, const qreal &mass, const QPointF &rotation)
{
    Ship * ship = new Ship(this);
    ship->setPosition(position);
    ship->setMass(mass);
    ship->setRotationAngle(0);

    _physicsManager->addShip(ship);

    emit shipAdded(ship);
}

void GameWorld::integrate(const qreal &timeStep)
{
    _physicsManager->integrate(timeStep);
}

void GameWorld::initPhysicsManager()
{
    _physicsManager = new PhysicsManager(this);
    _physicsManager->setGravityConst(6.673);
}

void GameWorld::destroyPhysicsManager()
{
    delete _physicsManager;
}

