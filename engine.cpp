#include "engine.h"

Engine::Engine(QObject *parent) : QObject(parent)
{
    initGame();
}

Engine::~Engine() {
    destroyGame();
}

void Engine::initGame()
{
    _game = new Game(this);

    emit gameChanged();
}

void Engine::destroyGame()
{
    delete _game;

    emit gameChanged();
}

Game *Engine::game() const
{
    return _game;
}
