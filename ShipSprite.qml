import QtQuick 2.0

import OrbitalTournament 1.0

Rectangle {
    property var model: null

    id: sprite

    x: engine.game.viewManager.translateCoordinate(model.position).x - radius
    y: engine.game.viewManager.translateCoordinate(model.position).y - radius
    width: radius * 2
    height: radius * 2

    radius: engine.game.viewManager.scaleValue(10)

    color: "red"

    onModelChanged: {
        spriteCanvas.requestPaint();
    }

    Connections{
        target: model

        onRotationAngleChanged: {
            spriteCanvas.requestPaint();
        }
    }

    Canvas {
        id: spriteCanvas

        property point center: Qt.point(width/2, height/2)

        x: -width/2
        y: -height/2
        width: model.shape.spriteBiggestSize
        height: model.shape.spriteBiggestSize

        onPaint:{
            var ctx = spriteCanvas.getContext('2d');
            ctx.save();
            ctx.clearRect(0, 0, spriteCanvas.width, spriteCanvas.height);
            ctx.translate(center.x, center.y);
            ctx.rotate(model.rotationAngle);
            drawPolygon(ctx);
            ctx.restore();

          }

        function drawPolygon(context) {
            context.strokeStyle = "black";

            context.beginPath();


            for(var i in model.shape.vertexesList) {
                var point = model.shape.vertexesList[i];
                var screenPoint = engine.game.viewManager.scalePointCoordinate(point.x, point.y);

                if(i === 0) {
                    context.moveTo(screenPoint.x, screenPoint.y)
                } else {
                    context.lineTo(screenPoint.x, screenPoint.y)
                }
            }

            context.closePath();

            context.stroke();
        }
    }

    VectorsOverlay {
        center: Qt.point(sprite.radius, sprite.radius)

        vectors: [
            {
                value: engine.game.viewManager.scalePointCoordinate(model.gravityVector),
                color: "cyan"
            },
            {
                value: engine.game.viewManager.scalePointCoordinate(model.velocityVector),
                color: "red"
            }
        ]

    }
}

