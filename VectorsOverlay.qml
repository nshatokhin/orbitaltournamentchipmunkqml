import QtQuick 2.0

Rectangle {
    id: root

    property point center: Qt.point(0, 0)

    // [ { value: value, color: color }, ... ]
    property var vectors: []

    x: center.x - radius
    y: center.y - radius

    width: radius * 2
    height: radius * 2

    color: "transparent"

    Canvas {
        id: vectorsCanvas
        anchors.fill: parent

        property point center: Qt.point(radius, radius)

        property point gravity: engine.game.viewManager.scalePointCoordinate(model.gravityVector)

        onGravityChanged: requestPaint();

        onPaint:{

            var ctx = vectorsCanvas.getContext('2d');

            ctx.save();

            ctx.clearRect(0, 0, vectorsCanvas.width, vectorsCanvas.height);

            drawVectors(ctx);



            ctx.restore();
          }

        function drawVector(context, vector) {
            context.strokeStyle = vector.color;

            context.beginPath();
            context.moveTo(center.x, center.y)
            context.lineTo(center.x + vector.value.x, center.y + vector.value.y)
            context.closePath();

            context.stroke();
        }

        function drawVectors(context) {
            for(var i=0;i<root.vectors.length;i++) {
                var vector = root.vectors[i];

                drawVector(context, vector);
            }
        }
    }

    onVectorsChanged: {
        updateRadius();

        vectorsCanvas.requestPaint();
    }

    function vectorLength(vector) {
        return Math.ceil( Math.sqrt( Math.pow(vector.value.x, 2) + Math.pow(vector.value.y, 2) ) );
    }

    function updateRadius() {
        if(vectors.length > 0) {
            var longestVector = vectors[0];

            for(var i=1;i<vectors.length;i++) {
                var vector = vectors[i];

                if(vectorLength(longestVector) < vectorLength(vector)) {
                    longestVector = vector;
                }
            }

            root.radius = vectorLength(longestVector);
        }
    }
}

