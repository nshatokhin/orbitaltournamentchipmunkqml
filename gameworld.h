#ifndef GAMEWORLD_H
#define GAMEWORLD_H

#include <QObject>
#include <QPointF>

class GravitySource;
class PhysicsManager;
class Ship;

class GameWorld : public QObject
{
    Q_OBJECT
public:
    explicit GameWorld(QObject *parent = 0);
    ~GameWorld();

    void addGravitySource(const QPointF &position, const qreal &radius, const qreal &mass, const bool &dynamic = false, const QPointF &velocity = QPointF());
    void addShip(const QPointF &position, const qreal &mass, const QPointF &rotation);

    void integrate(const qreal &timeStep);

signals:
    void gravitySourceAdded(GravitySource * gravitySource);
    void shipAdded(Ship * ship);

public slots:

protected:
    QList<GravitySource*> _gravitySources;

    PhysicsManager * _physicsManager;

    void initPhysicsManager();
    void destroyPhysicsManager();
};

#endif // GAMEWORLD_H
