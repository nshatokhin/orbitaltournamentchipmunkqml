#include "physicsmanager.h"

#include <QDebug>
#include <QtMath>

#include "objects/gravitysource.h"
#include "objects/ship.h"

qreal PhysicsManager::_gravityConst = 0;
QList<cpBody*> PhysicsManager::_gravitySources = QList<cpBody*>();

PhysicsManager::PhysicsManager(QObject *parent) : QObject(parent)
{
    _cpSpaceIterations = 20;

    _space = cpSpaceNew();
    cpSpaceSetIterations(_space, 20);
}

PhysicsManager::~PhysicsManager()
{
    cpSpaceFree(_space);
}

qreal PhysicsManager::gravityConst() const
{
    return _gravityConst;
}

void PhysicsManager::setGravityConst(const qreal &constant)
{
    _gravityConst = constant;
}

void PhysicsManager::addGravitySource(GravitySource *gravitySource, const bool &dynamic)
{
    cpBody* gravitySourceBody;

    if(dynamic) {
        gravitySourceBody = cpSpaceAddBody(_space, cpBodyNew(gravitySource->mass(), cpMomentForCircle(gravitySource->mass(), 0, gravitySource->radius(), cpvzero)));//cpBodyNewKinematic());
    } else {
        gravitySourceBody = cpSpaceAddBody(_space, cpBodyNewStatic());
    }

    cpBodySetPosition(gravitySourceBody, cpv(gravitySource->position().x(), gravitySource->position().y()));

    cpShape *shape = cpSpaceAddShape(_space, cpCircleShapeNew(gravitySourceBody, gravitySource->radius(), cpvzero));
    cpShapeSetElasticity(shape, 1.0f);
    cpShapeSetFriction(shape, 1.0f);

    cpBodySetUserData(gravitySourceBody, static_cast<cpDataPointer>(gravitySource));

    _gravitySources.append(gravitySourceBody);

    if(dynamic) {
        cpBodySetVelocity(gravitySourceBody, cpv(gravitySource->velocityVector().x(), gravitySource->velocityVector().y()));

        void (*updateFunc)(cpBody*, cpVect, cpFloat, cpFloat);
        updateFunc = gravityFunc<GravitySource>;
        cpBodySetVelocityUpdateFunc(gravitySourceBody, updateFunc);

        _gameObjects.append(gravitySourceBody);
    }
}

void PhysicsManager::addShip(Ship *ship)
{
    cpBody* shipBody;

    cpFloat moment = cpMomentForPoly(ship->mass(), ship->shape()->vertexCount(),
                                     ship->shape()->vertexes(), cpvzero, 0);
    shipBody = cpSpaceAddBody(_space, cpBodyNew(ship->mass(), moment));

    cpBodySetPosition(shipBody, cpv(ship->position().x(), ship->position().y()));

    cpShape *shape = cpSpaceAddShape(_space, cpPolyShapeNewRaw(shipBody, ship->shape()->vertexCount(),
                                                            ship->shape()->vertexes(), /*cpTransform(),*/ 0));
    cpShapeSetElasticity(shape, 1.0f);
    cpShapeSetFriction(shape, 1.0f);

    cpBodySetUserData(shipBody, static_cast<cpDataPointer>(ship));

    cpBodySetVelocity(shipBody, cpv(ship->velocityVector().x(), ship->velocityVector().y()));
    cpBodySetAngle(shipBody, ship->rotationAngle());
    cpSpaceReindexShapesForBody(_space, shipBody);

    void (*updateFunc)(cpBody*, cpVect, cpFloat, cpFloat);
    updateFunc = gravityFunc<Ship>;
    cpBodySetVelocityUpdateFunc(shipBody, updateFunc);

    _gameObjects.append(shipBody);
}

void PhysicsManager::integrate(const qreal &timeStep)
{
    cpSpaceStep(_space, timeStep);
}

// TODO: make friend non-member
template<typename T>
void PhysicsManager::gravityFunc(cpBody *body, cpVect, cpFloat damping, cpFloat dt)
{
    T * object = static_cast<T*>(cpBodyGetUserData(body));

    if(object) {
        cpVect bodyPos = cpBodyGetPosition(body);

        cpVect forces = cpvzero;

        for(int i=0;i<_gravitySources.length();i++) {
            cpBody * gsBody = _gravitySources[i];

            if(body == gsBody) {
                continue;
            }

            GravitySource * gsObject = static_cast<GravitySource*>(cpBodyGetUserData(gsBody));

            if(gsObject) {
                cpVect gravityPos = cpBodyGetPosition(gsBody);

                cpVect direction = cpvsub(gravityPos, bodyPos);
                cpFloat directionVectorLength = cpvlengthsq(direction);

                if(directionVectorLength == 0) {
                    continue;
                }

                qreal gravityAcceleration = _gravityConst * object->mass() * gsObject->mass() / directionVectorLength;

                cpVect gravityForce = cpvmult(cpvnormalize(direction), gravityAcceleration);

                forces = cpvadd(forces, gravityForce);
            }
        }

        cpBodyUpdateVelocity(body, forces, damping, dt);

        object->setPosition(cpBodyGetPosition(body));
        object->setGravityVector(forces);
        object->setVelocityVector(cpBodyGetVelocity(body));
        object->setRotationAngle(cpBodyGetAngle(body));
    }

}
