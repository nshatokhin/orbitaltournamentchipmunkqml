#ifndef ENGINE_H
#define ENGINE_H

#include <QObject>

#include "game.h"
#include "viewmanager.h"

class Engine : public QObject
{
    Q_OBJECT

    Q_PROPERTY( Game* game READ game NOTIFY gameChanged )

public:
    explicit Engine(QObject *parent = 0);
    ~Engine();

    Game* game() const;

signals:
    void gameChanged();

public slots:

protected:
    Game * _game;

    void initGame();
    void destroyGame();
};

#endif // ENGINE_H
