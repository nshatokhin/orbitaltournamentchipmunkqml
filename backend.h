#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QQuickItem>

class Backend : public QObject
{
    Q_OBJECT
public:
    explicit Backend(QObject *parent = 0);

    void registerTypes(const char *uri);

signals:

public slots:

private:
    int _versionMajor;
    int _versionMinor;
};

#endif // BACKEND_H
