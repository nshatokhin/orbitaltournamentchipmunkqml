#include "backend.h"

#include "game.h"
#include "gameworld.h"
#include "engine.h"
#include "controlmanager.h"
#include "viewmanager.h"

#include "objects/gravitysource.h"
#include "objects/ship.h"
#include "objects/ship/floatpoint.h"
#include "objects/ship/shape.h"

Backend::Backend(QObject *parent) : QObject(parent)
{
    _versionMajor = 1;
    _versionMinor = 0;
}

void Backend::registerTypes(const char *uri)
{
    qmlRegisterType<Game>(uri, _versionMajor, _versionMinor, "Game");
    qmlRegisterType<GameWorld>(uri, _versionMajor, _versionMinor, "GameWorld");
    qmlRegisterType<Engine>(uri, _versionMajor, _versionMinor, "Engine");

    qmlRegisterType<ControlManager>(uri, _versionMajor, _versionMinor, "ControlManager");
    qmlRegisterType<ViewManager>(uri, _versionMajor, _versionMinor, "ViewManager");

    qmlRegisterType<GravitySource>(uri, _versionMajor, _versionMinor, "GravitySource");

    qmlRegisterType<Ship>(uri, _versionMajor, _versionMinor, "Ship");
    qmlRegisterType<FloatPoint>(uri, _versionMajor, _versionMinor, "FloatPoint");
    qmlRegisterType<Shape>(uri, _versionMajor, _versionMinor, "Shape");
}

