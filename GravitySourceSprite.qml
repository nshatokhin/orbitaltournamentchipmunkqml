import QtQuick 2.0

Rectangle {
    property var model: null

    id: sprite

    x: engine.game.viewManager.translateCoordinate(model.position).x - radius
    y: engine.game.viewManager.translateCoordinate(model.position).y - radius
    width: radius * 2
    height: radius * 2

    radius: engine.game.viewManager.scaleValue(model.radius)

    color: "black"

    VectorsOverlay {
        center: Qt.point(sprite.radius, sprite.radius)

        vectors: [
            {
                value: engine.game.viewManager.scalePointCoordinate(model.gravityVector),
                color: "cyan"
            },
            {
                value: engine.game.viewManager.scalePointCoordinate(model.velocityVector),
                color: "red"
            }
        ]

    }
}

