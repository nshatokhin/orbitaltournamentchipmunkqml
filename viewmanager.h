#ifndef VIEWMANAGER_H
#define VIEWMANAGER_H

#include <QObject>
#include <QPointF>

class GravitySource;

class ViewManager : public QObject
{
    Q_OBJECT
public:
    explicit ViewManager(QObject *parent = 0);

    Q_INVOKABLE qreal scaleValue(const qreal &value) const;
    Q_INVOKABLE QPointF scalePointCoordinate(const QPointF &point) const;
    Q_INVOKABLE QPointF scalePointCoordinate(const qreal &x, const qreal &y) const;
    Q_INVOKABLE QPointF translateCoordinate(const QPointF &coordinate) const;
    Q_INVOKABLE QPointF rotatePoint(QPointF point, QPointF center, qreal angle) const;

    Q_INVOKABLE void setWorldCenter(const QPointF &center);

signals:
    void gravitySourceAdded(GravitySource* gravitySource);

public slots:
    void addGravitySource(const QPointF &position, const qreal &radius);

protected:
    QList<GravitySource*> _gravitySources;

    qreal _scale;
    QPointF _worldCenter;

    QPointF moveWorldCenter(const QPointF &point) const;

};

#endif // VIEWMANAGER_H
