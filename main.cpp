#include <QApplication>
#include <QQmlApplicationEngine>

#include "backend.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    Backend backend(&app);
    backend.registerTypes("OrbitalTournament");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
