import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2

import OrbitalTournament 1.0

ApplicationWindow {
    title: qsTr("Hello World")
    width: 640
    height: 480
    visible: true
    id: mainWindow

    Engine {
        id: engine

        Component.onCompleted: {

            engine.game.test()
        }
    }

    Item {
        anchors.fill: parent
        focus: true

        Keys.onPressed: {
            console.log(event.modifiers);
            engine.game.controlManager.keyboardPressEvent(event.key);
        }

        Keys.onReleased: {
            engine.game.controlManager.keyboardReleaseEvent(event.key);
        }
    }

    Connections {
        target: engine.game.gameWorld

        onGravitySourceAdded: {
            console.log("Gravity source added: " + gravitySource)
            spriteManager.createSpriteObject(gravitySource, "GravitySourceSprite.qml")
        }

        onShipAdded: {
            console.log("Ship added: " + ship)
            spriteManager.createSpriteObject(ship, "ShipSprite.qml")
        }
    }

    GameScene {
        id: gameScene

        Component.onCompleted: {
            engine.game.viewManager.setWorldCenter(Qt.point(mainWindow.width / 2, mainWindow.height / 2))
        }
    }

    SpriteManager {
        id: spriteManager
        scene: gameScene
    }
}
