#include "controlmanager.h"

#include <QDebug>

ControlManager::ControlManager(QObject *parent) : QObject(parent)
{

}

void ControlManager::keyboardPressEvent(const Qt::Key &key)
{
    qDebug() << "Press:" << key;
}

void ControlManager::keyboardReleaseEvent(const Qt::Key &key)
{
    qDebug() << "Release:" << key;
}

