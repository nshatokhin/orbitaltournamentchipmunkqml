#ifndef PHYSICSMANAGER_H
#define PHYSICSMANAGER_H

#include <QObject>
#include <QPointF>

#include <chipmunk/chipmunk.h>

class GameObject;
class GravitySource;
class Ship;

class PhysicsManager : public QObject
{
    Q_OBJECT
public:
    explicit PhysicsManager(QObject *parent = 0);
    ~PhysicsManager();

    qreal gravityConst() const;
    void setGravityConst(const qreal &constant);

    void addGravitySource(GravitySource * gravitySource, const bool &dynamic = false);
    void addShip(Ship * ship);

    void integrate(const qreal &timeStep);
signals:

public slots:

protected:
    cpSpace * _space;

    qint32 _cpSpaceIterations;

    static qreal _gravityConst;

    QList<cpBody*> _gameObjects;
    static QList<cpBody*> _gravitySources;

    template<typename T> static void gravityFunc(cpBody *body, cpVect gravity, cpFloat damping, cpFloat dt);

};

#endif // PHYSICSMANAGER_H
