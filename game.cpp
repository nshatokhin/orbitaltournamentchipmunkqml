#include "game.h"

#include <QPointF>

#include "gameworld.h"
#include "controlmanager.h"
#include "viewmanager.h"

Game::Game(QObject *parent) : QObject(parent)
{
    initGameWorld();
    initControlManager();
    initViewManager();

    _timer = new QTimer(this);
    connect(_timer, SIGNAL(timeout()), this, SLOT(timeout()));
    _timer->setInterval(1000/60);

    _timer->start();

}

Game::~Game()
{
    delete _timer;

    destroyGameWorld();
    destroyViewManager();
}

void Game::initGameWorld()
{
    _gameWorld = new GameWorld(this);

    emit gameWorldChanged();
}

void Game::destroyGameWorld()
{
    delete _gameWorld;

    emit gameWorldChanged();
}

GameWorld *Game::gameWorld() const
{
    return _gameWorld;
}

ControlManager *Game::controlManager() const
{
    return _controlManager;
}

void Game::initViewManager()
{
    _viewManager = new ViewManager(this);

    emit viewManagerChanged();
}

void Game::destroyViewManager()
{
    delete _viewManager;

    emit viewManagerChanged();
}

void Game::initControlManager()
{
    _controlManager = new ControlManager(this);

    emit controlManagerChanged();
}

void Game::destroyControlManager()
{
    delete _controlManager;

    emit controlManagerChanged();
}

ViewManager *Game::viewManager() const
{
    return _viewManager;
}

void Game::test()
{
    _gameWorld->addGravitySource(QPointF(0, 0), 500, 50000, false);
    _gameWorld->addGravitySource(QPointF(900, 700), 200, 2000, true, QPointF(700, -600));
    _gameWorld->addGravitySource(QPointF(-1900, -1000), 30, 3000, true);
    _gameWorld->addGravitySource(QPointF(2000, -1500), 10, 100, true);

    _gameWorld->addShip(QPointF(-2000, -1500), 100, QPointF(0, 0));
    _gameWorld->addShip(QPointF(-1500, -1000), 100, QPointF(-1000, 0));
}

void Game::integrate(const qreal &timeStep)
{
    _gameWorld->integrate(timeStep);
}

void Game::timeout()
{
    integrate(1.0/60);
}

