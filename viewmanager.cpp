#include "viewmanager.h"

#include <QDebug>
#include <QtMath>

#include "objects/gravitysource.h"

ViewManager::ViewManager(QObject *parent) : QObject(parent)
{
    _scale = 0.1;
}

qreal ViewManager::scaleValue(const qreal &value) const
{
    return value * _scale;
}

QPointF ViewManager::translateCoordinate(const QPointF &coordinate) const
{
    return moveWorldCenter(scalePointCoordinate(coordinate));
}

void ViewManager::setWorldCenter(const QPointF &center)
{
    _worldCenter = center;
    qDebug() << center;
}

void ViewManager::addGravitySource(const QPointF &position, const qreal &radius)
{
    GravitySource * gravitySource = new GravitySource(this);
    gravitySource->setPosition(position);
    gravitySource->setRadius(radius);

    _gravitySources.append(gravitySource);

    emit gravitySourceAdded(gravitySource);
}

QPointF ViewManager::moveWorldCenter(const QPointF &point) const
{
    return point + _worldCenter;
}

QPointF ViewManager::scalePointCoordinate(const QPointF &point) const
{
    return point * _scale;
}

QPointF ViewManager::scalePointCoordinate(const qreal &x, const qreal &y) const
{
    return QPointF(x * _scale, y * _scale);
}

QPointF ViewManager::rotatePoint(QPointF point, QPointF center, qreal angle) const
{
  qreal s = qSin(angle);
  qreal c = qCos(angle);

  // translate point back to origin:
  point.setX(point.x() - center.x());
  point.setY(point.y() - center.y());

  // rotate point
  qreal xnew = point.x() * c - point.y() * s;
  qreal ynew = point.x() * s + point.y() * c;

  // translate point back:
  point.setX(xnew + center.x());
  point.setY(ynew + center.y());
  return point;
}
