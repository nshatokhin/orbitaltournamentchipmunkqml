#ifndef GAME_H
#define GAME_H

#include <QObject>
#include <QTimer>

class GameWorld;
class ControlManager;
class ViewManager;

class Game : public QObject
{
    Q_OBJECT

    Q_PROPERTY(GameWorld* gameWorld READ gameWorld NOTIFY gameWorldChanged )
    Q_PROPERTY(ViewManager* viewManager READ viewManager NOTIFY viewManagerChanged )
    Q_PROPERTY(ControlManager* controlManager READ controlManager NOTIFY controlManagerChanged )

public:
    explicit Game(QObject *parent = 0);
    ~Game();

    GameWorld * gameWorld() const;

    ControlManager * controlManager() const;
    ViewManager * viewManager() const;

    Q_INVOKABLE void test();

    void integrate(const qreal &timeStep);

signals:
    void gameWorldChanged();
    void controlManagerChanged();
    void viewManagerChanged();

public slots:
    void timeout();

protected:
    GameWorld * _gameWorld;
    ControlManager * _controlManager;
    ViewManager * _viewManager;

    void initGameWorld();
    void destroyGameWorld();

    void initViewManager();
    void destroyViewManager();

    void initControlManager();
    void destroyControlManager();

    QTimer * _timer;
};

#endif // GAME_H
