#include "gameobject.h"

GameObject::GameObject(QObject *parent) : QObject(parent)
{

}

qreal GameObject::mass() const
{
    return _mass;
}

void GameObject::setMass(const qreal &mass)
{
    _mass = mass;

    emit massChanged();
}

QPointF GameObject::position() const
{
    return _position;
}

void GameObject::setPosition(const QPointF &pos)
{
    _position = pos;

    emit positionChanged();
}

void GameObject::setPosition(const cpVect &pos)
{
    setPosition(QPointF(pos.x, pos.y));
}

QPointF GameObject::gravityVector() const
{
    return _gravityVector;
}

void GameObject::setGravityVector(const QPointF &gravity)
{
    _gravityVector = gravity;

    emit gravityChanged();
}

void GameObject::setGravityVector(const cpVect &gravity)
{
    setGravityVector(QPointF(gravity.x, gravity.y));
}

QPointF GameObject::velocityVector() const
{
    return _velocityVector;
}

void GameObject::setVelocityVector(const QPointF &velocity)
{
    _velocityVector = velocity;

    emit velocityChanged();
}

void GameObject::setVelocityVector(const cpVect &velocity)
{
    setVelocityVector(QPointF(velocity.x, velocity.y));
}

QPointF GameObject::rotationVector() const
{
    return _rotationVector;
}

void GameObject::setRotationVector(const QPointF &rotation)
{
    _rotationVector = rotation;

    emit rotationVectorChanged();
}

void GameObject::setRotationVector(const cpVect &rotation)
{
    setRotationVector(QPointF(rotation.x, rotation.y));
}

qreal GameObject::rotationAngle() const
{
    return _rotationAngle;
}

void GameObject::setRotationAngle(const qreal &angle)
{
    if(_rotationAngle != angle)
    {
        _rotationAngle = angle;
        emit rotationAngleChanged();
    }
}


