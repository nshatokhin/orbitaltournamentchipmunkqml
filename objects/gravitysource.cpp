#include "gravitysource.h"

GravitySource::GravitySource(QObject *parent) : GameObject(parent)
{

}

qreal GravitySource::radius() const
{
    return _radius;
}

void GravitySource::setRadius(const qreal &radius)
{
    _radius = radius;

    emit radiusChanged();
}
