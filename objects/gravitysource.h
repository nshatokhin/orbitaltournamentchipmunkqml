#ifndef GRAVITYSOURCE_H
#define GRAVITYSOURCE_H

#include "gameobject.h"

class GravitySource : public GameObject
{
    Q_OBJECT

    Q_PROPERTY(qreal radius READ radius WRITE setRadius NOTIFY radiusChanged)
public:
    explicit GravitySource(QObject *parent = 0);

    qreal radius() const;
    void setRadius(const qreal &radius);

signals:
    void radiusChanged();

public slots:

protected:
    qreal _radius;
};

#endif // GRAVITYSOURCE_H
