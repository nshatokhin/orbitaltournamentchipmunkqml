#include "shape.h"

#include <limits>

#include <QPointF>

Shape::Shape(QObject *parent) : QObject(parent)
{
    _vertexes = NULL;
}

Shape::Shape(Shape &copy) : QObject(copy.parent())
{
    setSprite(copy.sprite());
    setVertexes(copy.vertexCount(), copy.vertexes());
}

Shape::~Shape()
{
    destroyVertexes();
}

QString Shape::sprite() const
{
    return _sprite;
}

void Shape::setSprite(const QString &sprite)
{
    _sprite = sprite;
}

quint32 Shape::vertexCount()
{
    return _vertexCount;
}

cpVect *Shape::vertexes()
{
    return _vertexes;
}

void Shape::setVertexes(quint32 count, cpVect *vertexes)
{
    destroyVertexes();

    _vertexCount = count;

    _vertexes = new cpVect[_vertexCount];

    for(quint32 i=0;i<_vertexCount;i++) {
        _vertexes[i] = vertexes[i];
        _vertexesList.append(new FloatPoint(_vertexes[i].x, _vertexes[i].y));
    }

    calculateSize();

    emit vertexesChanged();
}

QQmlListProperty<FloatPoint> Shape::vertexesList()
{
    return QQmlListProperty<FloatPoint>(this, _vertexesList);
}

qreal Shape::spriteWidth() const
{
    return _width;
}

qreal Shape::spriteHeight() const
{
    return _height;
}

qreal Shape::spriteBiggestSize() const
{
    return qMax<qreal>(_width, _height);
}

QPointF Shape::spriteCenter() const
{
    return _spriteCenter;
}

void Shape::setSpriteCenter(const QPointF &spriteCenter)
{
    if(_spriteCenter != spriteCenter)
    {
        _spriteCenter = spriteCenter;
        emit spriteCenterChanged(_spriteCenter);
    }
}

void Shape::setSpriteCenter(const cpVect &spriteCenter)
{
    QPointF point = QPointF(spriteCenter.x, spriteCenter.y);

    if(_spriteCenter != point)
    {
        _spriteCenter = point;
        emit spriteCenterChanged(_spriteCenter);
    }
}

void Shape::calculateSize()
{
    qreal minX = std::numeric_limits<double>::max(),
          minY = std::numeric_limits<double>::max(),
          maxX = std::numeric_limits<double>::min(),
          maxY = std::numeric_limits<double>::min();

    for(qint32 i=0;i<_vertexesList.count();i++) {
        FloatPoint * point = _vertexesList[i];

        if(point->x() < minX) {
            minX = point->x();
        }

        if(point->y() < minY) {
            minY = point->y();
        }

        if(point->x() > maxX) {
            maxX = point->x();
        }

        if(point->y() > maxY) {
            maxY = point->y();
        }
    }

    _width = qAbs<qreal>(minX) + qAbs<qreal>(maxX);
    _height = qAbs<qreal>(minY) + qAbs<qreal>(maxY);

    emit spriteSizeChanged(_width, _height);
    emit spriteBiggestSizeChanged(spriteBiggestSize());
}

void Shape::destroyVertexes()
{
    if(_vertexes != NULL) {
        delete[] _vertexes;
        _vertexes = NULL;
    }

    qDeleteAll(_vertexesList);
    _vertexesList.clear();
}
