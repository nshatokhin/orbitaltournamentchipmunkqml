#ifndef SHAPE_H
#define SHAPE_H

#include <QObject>
#include <QPointF>
#include <QQmlListProperty>

#include <chipmunk/chipmunk.h>

#include "objects/ship/floatpoint.h"

class Shape : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QQmlListProperty<FloatPoint> vertexesList READ vertexesList NOTIFY vertexesChanged)
    Q_PROPERTY(qreal spriteWidth READ spriteWidth NOTIFY spriteSizeChanged)
    Q_PROPERTY(qreal spriteHeight READ spriteHeight NOTIFY spriteSizeChanged)
    Q_PROPERTY(qreal spriteBiggestSize READ spriteBiggestSize NOTIFY spriteBiggestSizeChanged)
    Q_PROPERTY(QPointF spriteCenter READ spriteCenter NOTIFY spriteCenterChanged)
public:
    explicit Shape(QObject *parent = 0);
    Shape(Shape &copy);
    ~Shape();

    QString sprite() const;
    void setSprite(const QString &sprite);

    quint32 vertexCount();
    cpVect *vertexes();
    void setVertexes(quint32 count, cpVect *vertexes);
    QQmlListProperty<FloatPoint> vertexesList();

    qreal spriteWidth() const;
    qreal spriteHeight() const;
    qreal spriteBiggestSize() const;

    QPointF spriteCenter() const;
    void setSpriteCenter(const QPointF &spriteCenter);
    void setSpriteCenter(const cpVect &spriteCenter);

signals:
    void vertexesChanged();
    void spriteSizeChanged(const qreal &width, const qreal &height);
    void spriteBiggestSizeChanged(const qreal &size);
    void spriteCenterChanged(const QPointF &spriteCenter);

public slots:

protected:
    QString _sprite;
    quint32 _vertexCount;
    cpVect *_vertexes;
    QList<FloatPoint*> _vertexesList;
    qreal _width;
    qreal _height;
    QPointF _spriteCenter;

    void calculateSize();
    void destroyVertexes();
};
Q_DECLARE_METATYPE(QQmlListProperty<FloatPoint>)

#endif // SHAPE_H
