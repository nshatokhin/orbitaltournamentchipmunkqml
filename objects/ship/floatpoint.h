#ifndef FLOATPOINT_H
#define FLOATPOINT_H

#include <QObject>

class FloatPoint : public QObject
{
    Q_OBJECT

    Q_PROPERTY(qreal x READ x WRITE setX NOTIFY xChanged)
    Q_PROPERTY(qreal y READ y WRITE setY NOTIFY yChanged)
public:
    explicit FloatPoint(qreal x = 0, qreal y = 0, QObject *parent = 0);

    qreal x() const;
    void setX(const qreal &x);

    qreal y() const;
    bool setY(const qreal &y);

signals:
    void xChanged(const qreal &x);
    void yChanged(const qreal &y);

private:
    qreal _x;
    qreal _y;
};

#endif // FLOATPOINT_H
