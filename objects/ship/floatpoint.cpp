#include "floatpoint.h"

FloatPoint::FloatPoint(qreal x, qreal y, QObject *parent) :
    QObject(parent), _x(x), _y(y)
{

}

qreal FloatPoint::x() const
{
    return _x;
}

void FloatPoint::setX(const qreal &x)
{
    if(_x != x) {
        _x = x;

        emit xChanged(_x);

        //return true;
    }

    //return false;
}

qreal FloatPoint::y() const
{
    return _y;
}

bool FloatPoint::setY(const qreal &y)
{
    if(_y != y) {
        _y = y;

        emit yChanged(_y);

        return true;
    }

    return false;
}


