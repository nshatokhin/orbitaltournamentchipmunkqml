#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <QObject>
#include <QPointF>

#include <chipmunk/chipmunk.h>

class GameObject : public QObject
{
    Q_OBJECT

    Q_PROPERTY(qreal mass READ mass WRITE setMass NOTIFY massChanged)
    Q_PROPERTY(QPointF position READ position WRITE setPosition NOTIFY positionChanged)
    Q_PROPERTY(QPointF gravityVector READ gravityVector WRITE setGravityVector NOTIFY gravityChanged)
    Q_PROPERTY(QPointF velocityVector READ velocityVector WRITE setVelocityVector NOTIFY velocityChanged)
    Q_PROPERTY(QPointF rotationVector READ rotationVector WRITE setRotationVector NOTIFY rotationVectorChanged)
    Q_PROPERTY(qreal rotationAngle READ rotationAngle WRITE setRotationAngle NOTIFY rotationAngleChanged)
public:
    explicit GameObject(QObject *parent = 0);

    qreal mass() const;
    void setMass(const qreal &mass);

    QPointF position() const;
    void setPosition(const QPointF &pos);
    void setPosition(const cpVect &pos);

    QPointF gravityVector() const;
    void setGravityVector(const QPointF &gravity);
    void setGravityVector(const cpVect &gravity);

    QPointF velocityVector() const;
    void setVelocityVector(const QPointF &velocity);
    void setVelocityVector(const cpVect &velocity);

    QPointF rotationVector() const;
    void setRotationVector(const QPointF &rotation);
    void setRotationVector(const cpVect &rotation);

    qreal rotationAngle() const;
    void setRotationAngle(const qreal &angle);

signals:
    void massChanged();
    void positionChanged();
    void gravityChanged();
    void velocityChanged();
    void rotationVectorChanged();
    void rotationAngleChanged();

public slots:

protected:
    qreal _mass;
    qreal _rotationAngle;

    QPointF _position;
    QPointF _gravityVector;
    QPointF _velocityVector;
    QPointF _rotationVector;
};

#endif // GAMEOBJECT_H
