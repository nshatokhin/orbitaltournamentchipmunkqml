#ifndef SHIP_H
#define SHIP_H

#include "gameobject.h"

#include "ship/shape.h"

class AbstractController;

class Ship : public GameObject
{
    Q_OBJECT

    Q_PROPERTY(Shape* shape READ shape NOTIFY shapeChanged)
public:
    explicit Ship(QObject *parent = 0, AbstractController *controller = nullptr);

    qreal angle() const;
    void setAngle(const qreal &angle);

    QString sprite() const;

    Shape *shape();

    void setController(AbstractController *controller);

signals:
    void shapeChanged();

public slots:
    void startEngine();
    void stopEngine();

protected:
    qreal _angle;

    Shape _shape;

    AbstractController *_controller;
};

#endif // SHIP_H
