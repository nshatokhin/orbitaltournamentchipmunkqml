#include "ship.h"

Ship::Ship(QObject *parent, AbstractController *controller) :
    GameObject(parent)
{
    _shape.setSprite("://images/sprites/ship.svg");

    cpVect cpVert[] = {cpv(-150.0f, 200.0f), cpv(0.0f, -200.0f), cpv(150.0f, 200.0f)};
    _shape.setVertexes(3, cpVert);
    _shape.setSpriteCenter(cpv(0.0f, 0.0f));

    setController(controller);
}

qreal Ship::angle() const
{
    return _angle;
}

void Ship::setAngle(const qreal &angle)
{
    _angle = angle;
}

QString Ship::sprite() const
{
    return _shape.sprite();
}

Shape *Ship::shape()
{
    return &_shape;
}

void Ship::setController(AbstractController *controller)
{
    _controller = controller;

    // connect to signals
}

void Ship::startEngine()
{

}

void Ship::stopEngine()
{

}
