#ifndef KEYBOARDCONTROLLER_H
#define KEYBOARDCONTROLLER_H

#include <QMap>

#include "abstractcontroller.h"

class KeyboardController : public AbstractController
{
public:
    KeyboardController(const QString &ID, AbstractController *parent = 0);

    QString id() const;
    void setId(const QString &id);

    bool hasKey(const Qt::Key &key) const;

public slots:
    void pressEvent(const Qt::Key &key);
    void releaseEvent(const Qt::Key &key);

signals:
    void idChanged();

protected:
    QString _id;
    QMap<Qt::Key, AbstractController::Actions> _actions;
};

#endif // KEYBOARDCONTROLLER_H
