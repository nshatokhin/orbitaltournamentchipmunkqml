#ifndef ABSTRACTCONTROLLER_H
#define ABSTRACTCONTROLLER_H

#include <QObject>

class AbstractController : public QObject
{
    Q_OBJECT
public:
    explicit AbstractController(QObject *parent = 0);

    enum Actions {
        START_ENGINE = 0,
        STOP_ENGINE,
        START_ROTATION,
        STOP_ROTATION,
    };

signals:
    void startEngine();
    void stopEngine();

    void startRotation();
    void stopRotation();

public slots:

protected:
    void emitCommand(const Actions &command);

};

#endif // ABSTRACTCONTROLLER_H
