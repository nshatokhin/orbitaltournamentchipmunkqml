#include "keyboardcontroller.h"

KeyboardController::KeyboardController(const QString &ID, AbstractController *parent) : AbstractController(parent),
    _id(ID)
{

}

QString KeyboardController::id() const
{
    return _id;
}

void KeyboardController::setId(const QString &id)
{
    _id = id;
}

bool KeyboardController::hasKey(const Qt::Key &key) const
{
    return _actions.contains(key);
}

void KeyboardController::pressEvent(const Qt::Key &key)
{

}

void KeyboardController::releaseEvent(const Qt::Key &key)
{

}
