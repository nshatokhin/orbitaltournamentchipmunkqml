#include "abstractcontroller.h"

AbstractController::AbstractController(QObject *parent) : QObject(parent)
{

}

void AbstractController::emitCommand(const AbstractController::Actions &command)
{
    switch (command) {
    case START_ENGINE:
        emit startEngine();
        break;
    case STOP_ENGINE:
        emit stopEngine();
        break;
    case START_ROTATION:
        emit startRotation();
        break;
    case STOP_ROTATION:
        emit stopRotation();
        break;
    default:
        break;
    }
}
