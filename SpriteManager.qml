import QtQuick 2.0

Rectangle {
    property var scene: null

    function createSpriteObject(object, view) {
        var component = Qt.createComponent(view);

        var sprite = component.createObject(scene, {"model": object});
        if (sprite == null) {
            // Error Handling
            console.log("Error creating object");
        }

    }
}

