TEMPLATE = app

QT += qml quick widgets

LIBS += -L/usr/local/lib/ -lchipmunk

SOURCES += main.cpp \
    backend.cpp \
    engine.cpp \
    game.cpp \
    gameworld.cpp \
    viewmanager.cpp \
    objects/gameobject.cpp \
    physicsmanager.cpp \
    objects/ship.cpp \
    objects/gravitysource.cpp \
    objects/ship/shape.cpp \
    objects/ship/floatpoint.cpp \
    controlmanager.cpp \
    controllers/abstractcontroller.cpp \
    controllers/keyboardcontroller.cpp

RESOURCES += qml.qrc \
    sprites.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    backend.h \
    engine.h \
    game.h \
    gameworld.h \
    viewmanager.h \
    objects/gameobject.h \
    physicsmanager.h \
    objects/ship.h \
    objects/gravitysource.h \
    objects/ship/shape.h \
    objects/ship/floatpoint.h \
    controlmanager.h \
    controllers/abstractcontroller.h \
    controllers/keyboardcontroller.h
