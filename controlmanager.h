#ifndef CONTROLMANAGER_H
#define CONTROLMANAGER_H

#include <QKeyEvent>
#include <QObject>

#include "controllers/keyboardcontroller.h"

class ControlManager : public QObject
{
    Q_OBJECT

public:
    explicit ControlManager(QObject *parent = 0);

signals:

public slots:
    void keyboardPressEvent(const Qt::Key &key);
    void keyboardReleaseEvent(const Qt::Key &key);

protected:
    QMap<QString, KeyboardController *> _keyboardControllers;
};

#endif // CONTROLMANAGER_H
